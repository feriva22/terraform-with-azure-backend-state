# Output the Public IP for the VM
output "instance_ip_addr" {
  value       = azurerm_public_ip.main.ip_address
  description = "The public IP address of the main server instance."
}