# terraform-with-azure



## Getting started


This project will be provisioning infrastructure like **VM, Network groups** in Azure Cloud using Terraform, and use Azure Blob Storage as **Backend State for terraform**.\
At least you must have knowledge about :
- Terraform flow and syntax
- Azure cloud operation
- Editor code like Visual studio code
- Gitlab CI/CD feature (pipeline, variables)

## Get credential before start
This project will need 4 environment variables setup for CI/CD, the environment is :
- ```ARM_ACCESS_KEY```: The Access Key we got from the Storage Account.
- ```ARM_CLIENT_ID```: The Client ID we got from the App Registration.
- ```ARM_CLIENT_SECRET```: The Secret we’ve created in the App Registration.
- ```ARM_SUBSCRIPTION_ID```: Your Subscription’s ID.
- ```ARM_TENANT_ID```: The Tenant ID we got from the App Registration.
- ```TF_VAR_DEFAULT_SSHKEY```: Your public SSH key.

For ```ARM_ACCESS_KEY``` you can get it from **Access Key** section in you Storage Account.\
For ```ARM_CLIENT_ID``` and ```ARM_CLIENT_SECRET``` ```ARM_TENANT_ID``` you can create first one at App Registrations menu.\
For ```TF_VAR_DEFAULT_SSHKEY``` place your public key ssh on your local machine for remote the server created.

## How it works

You can get the full flow with this article , [Click here](https://rdvansloten.medium.com/terraform-azure-gitlab-ci-part-1-basic-concepts-288050e06bd8)

