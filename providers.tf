provider "azurerm" {
  features {}
}
# We strongly recommend using the required_providers block to set the
# Azure Provider source and version being used
terraform {
  # Azure Provider
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.0.0"
    }
  }

  #Backend State
  backend "azurerm" {
    resource_group_name  = "learn-azure"
    storage_account_name = "learnterraform"
    container_name       = "terraform-state"
    key                  = "test.terraform.tfstate"
  }
}
